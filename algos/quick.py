
def qsort(arr):
    return qsort_helper(arr, 0, len(arr)-1)

def qsort_helper(arr, first, last):
    if first < last:
        splitpoint = partition(arr, first, last)

        qsort_helper(arr, first, splitpoint-1)
        qsort_helper(arr, splitpoint+1, last)
    return arr


def partition(arr, first, last):
    pivot = arr[first]
    leftmark = first + 1
    rightmark = last


    done = False
    while not done:
        while arr[leftmark] < pivot and leftmark < rightmark:
            leftmark += 1

        while arr[rightmark] >= pivot and rightmark >= leftmark:
            rightmark -= 1

        if leftmark > rightmark:
            # crossed.
            done = True
        else:
            arr[leftmark], arr[rightmark] = arr[rightmark], arr[leftmark]

        
    arr[first], arr[rightmark] = arr[rightmark], arr[first]

    return rightmark
        



def main():
    print(qsort([45,21,2,0,69,10, -1]))

if __name__ == '__main__':
    main()