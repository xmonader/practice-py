class Node:

    def __init__(self, data):

        self.left = None
        self.right = None
        self.data = data


    def __str__(self):
        return f"(data: {self.data} left:{self.left} right:{self.right})"
    
    __repr__ = __str__
    
    def insert(self, data):
# Compare the new value with the parent node
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.left = Node(data)
                else:
                    self.left.insert(data)
            elif data > self.data:
                if self.right is None:
                    self.right = Node(data)
                else:
                    self.right.insert(data)
        else:
            self.data = data



    def inorderprint(self):
        if self.left:
            self.left.inorderprint()
        print(self.data)
        if self.right:
            self.right.inorderprint()

    def postorderprint(self):
        if self.left:
            self.left.postorderprint()
        if self.right:
            self.right.postorderprint()

        print(self.data)


    def preorderprint(self):
        print(self.data)

        if self.left:
            self.left.preorderprint()
        if self.right:
            self.right.preorderprint()


    def invert(self, node):
        if node:
            newleft,  newright = self.invert(node.right), self.invert(node.left)

            node.left = newleft
            node.right = newright

        return node

    def size(self):
        q = []
        res = 0
        q.append(self)
        while q:
            n = q.pop(0)
            if n:
                res += 1
                q.append(n.left)
                q.append(n.right)
        
        return res

    def bfs(self):
        q = []
        start = self
        q.append(self)
        while q:
            n = q.pop(0)
            print(n.data)
            if n.left: 
                q.append(n.left)
            if n.right:
                q.append(n.right)



import random
def main():
    # Use the insert method to add nodes
    root = Node(1)

    root.left = Node(2)

    root.right = Node(3)

    root.left.left = Node(4)
    root.left.right = Node(5)


    print("IN ORDER:")
    root.inorderprint()
    print("POST ORDER:")
    root.postorderprint()

    print("PRE ORDER:")
    root.preorderprint()

    print(root)
    print("BFS: ")
    root.bfs()

    print("ROOT: ", root)

    # inv = root.invert(root)
    # print("INVERTED: ", inv)

    print(root.size())

if __name__ == '__main__':
    main()
