def radix(arr):
    N = len(arr)
    bins = [0]*N
    maxnum = max(arr)

    num = maxnum
    tenth = 10
    passes = 0
    while num // tenth != 0:
        for i in range(N):
            cur = arr[i]
            # import ipdb; ipdb.set_trace()
            bins[cur%tenth] = cur
        print(bins)
        num //= tenth
        tenth *= 10

        arr = bins
 

    return arr

print(radix([10,21, 11,122,24,4523,314]))

