import heapq

def heapsort(arr):
    h = []
    for el in arr:
        heapq.heappush(h, el)
    while h:
        yield heapq.heappop(h)


def main():
    import random
    arr = [random.randrange(50) for _ in range(10)]
    print(arr)
    print(list(heapsort(arr)))

if __name__ == '__main__':
    main()