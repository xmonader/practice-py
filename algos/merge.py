def merge(arr):

    if len(arr) == 1:
        return arr
    


    mid = len(arr)//2
    lefthalf = arr[:mid]
    righthalf = arr[mid:]

    lefthalf = merge(lefthalf)
    righthalf = merge(righthalf)

    i = 0
    j = 0
    k = 0

    while i<len(lefthalf) and j<len(righthalf):
        if lefthalf[i] < righthalf[j]:
            arr[k] = lefthalf[i]
            i += 1
        else:
            arr[k] = righthalf[j]
            j += 1
        k += 1
    
    while i <len(lefthalf):
        arr[k] = lefthalf[i]
        i += 1
        k += 1
    
    while j < len(righthalf):
        arr[k] = righthalf[j]
        j += 1
        k += 1
    return arr
print(merge([3,4,9,0, 2, 1]))