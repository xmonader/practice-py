# def bubble(arr):
#     for _pass in range(len(arr)):
#         for i in range(1, len(arr)):
#             # asc, arr[i] > arr[i-1] desc.
#             if arr[i] < arr[i-1]:
#                 tmp = arr[i-1]
#                 arr[i-1] = arr[i]
#                 arr[i] = tmp
#         print("After pass: ", _pass, " ==> ", arr)
#     return arr



def bubble(arr):
    for pas in range(len(arr)):
        for i in range(1, len(arr)):
            if arr[i-1] > arr[i]:
                arr[i], arr[i-1] = arr[i-1], arr[i]
        print(f"After pass {pas}: arr {arr}")
bubble([1,2,6,0,3])
