def insertion(arr):
    for i in range(1, len(arr)):
        j = i - 1
        key = arr[i]


# 2 3 7 1

# i=3 => current 1
# j = 2

# 2 3 1 7
# 2 1 3 7
# 1 2 3 7
        while j >= 0 and key < arr[j]:
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = key # j+1 might reach 0    
    return arr


print(insertion([1,2,19,3,17,14]))