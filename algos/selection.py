def selection(arr):
    for i in range(len(arr)):
        _minidx = i
        for j in range(i, len(arr)):
            print(arr, " I: ", i, " J: ", j, " MIN: ", _minidx)
            if arr[j] < arr[_minidx]:
                _minidx = j
        arr[i], arr[_minidx] = arr[_minidx], arr[i]
        print("Found min: ", arr[_minidx], ", at idx: ", _minidx)
    
    return arr


print(selection([1,2,19,3,17,14]))


# def selection(arr):
#     for i in range(len(arr)):
#         smallest = i
#         for j in range(j, len(arr)):
#             if arr[j] < arr[i]:
#                 smallest = arr[j]
#         arr[i], arr[smallest] = arr[smallest], arr[i]
        
# selection([1,2,6,0,3])
