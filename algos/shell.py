def shell(arr):
    N = len(arr)
    gap = N//2

    while gap > 0:
        for i in range(gap, N):
            key = arr[i]
            j = i
            while j >= gap and key < arr[j-gap]:
                arr[j] = arr[j-gap]
                j -= gap
            arr[j] = key
        gap //= 2
    return arr
print(shell([1,2,19,3,17,14]))