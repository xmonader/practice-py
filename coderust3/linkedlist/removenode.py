from linkedlist import Node, LinkedList



def remove_node(ll, val):
    cur = ll.head
    while cur and cur.next:
        if cur.next.data == val:
            cur.next = cur.next.next
        cur = cur.next
    return ll.head

def main():
    ll = LinkedList(alist=[1,2,3,4,5])
    print(remove_node(ll, 3))

if __name__ == '__main__':
    main()