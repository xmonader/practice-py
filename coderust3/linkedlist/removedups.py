from linkedlist import Node, LinkedList


def removedups(ll):
    seen = set()
    cur = ll.head
    newl = LinkedList()
    while cur:
        if cur.data not in seen:
            newl.append(cur.data)
            seen.add(cur.data)
        cur = cur.next
    return newl 


# public ListNode deleteDuplicates(ListNode head) {
#     ListNode current = head;
#     while (current != null && current.next != null) {
#         if (current.next.val == current.val) {
#             current.next = current.next.next;
#         } else {
#             current = current.next;
#         }
#     }
#     return head;
# }

def main():
    ll = LinkedList([1,2,3,4,4,5,5,9])
    newl = removedups(ll)
    print(newl)

if __name__ == '__main__':
    main()