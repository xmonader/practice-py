from linkedlist import Node, LinkedList



def reverse_linkedlist(ll):
    prev = None
    cur = ll.head
    next = None

    while cur:
        next = cur.next
        cur.next = prev
        prev = cur
        cur = next

    return prev

def main():
    ll = LinkedList(alist=[1,2,3,4,5])
    print(reverse_linkedlist(ll))

if __name__ == '__main__':
    main()