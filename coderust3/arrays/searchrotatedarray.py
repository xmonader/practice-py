
def bsearch(arr, el):
    low = 0
    high = len(arr)

    while low < high:
        mid = low+ (high-low)//2
        midel = arr[mid]
        if midel == el:
            return mid
        elif el > midel:
            low = mid
        else:
            high = mid
    return -1

def searchrotatedarray(arr, num):
    pivot = find_pivot(arr)

    idx = bsearch(arr[pivot:], num) 
    if not idx:
        idx = bsearch(arr[:pivot], num)
    else:
        idx += pivot

    return idx

def find_pivot(arr):
    low = arr[0]
    high = len(arr) - 1
    mid = low + (high-low)//2

    while mid > 0:
        # check if order is maintained
        if arr[mid+1] < arr[mid]:
            mid += 1
            break
        mid -= 1
    
    print("pivot is: ", mid)
    return mid

arr = [5, 70, 80, 1, 2, 3, 4]
find_pivot(arr)
print(searchrotatedarray(arr, 3))

# 1 2 3 4 5 70 80

# 5 70 80 1 2 3 4 
