def quicksort(nums):
    if nums == []:
        return []
    if len(nums) == 1:
        return nums
    
    x, *xs = nums
    less = [a for a in nums if a < x]
    greater = [b for b in nums if b > x]

    return quicksort(less) + [x] + quicksort(greater)


def quicksort_iter(nums):
    pass


print(quicksort([3,4,5,1,2,0, 9]))