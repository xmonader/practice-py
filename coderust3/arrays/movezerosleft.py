def movezerosleft(arr):
    res = []

    j = len(arr) - 1
    while j > 0:
        if arr[j] > 0:
            res.append(arr[j])
        j -= 1
    
    res.reverse()
    zeroes = len(arr) - len(res)
    for i in range(zeroes):
        res.insert(0, 0)

    return res

print(movezerosleft([0,1,0,3,12]))
