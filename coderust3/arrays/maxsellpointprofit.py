# Input: [7,1,5,3,6,4]
# Output: 5
# Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
#              Not 7-1 = 6, as selling price needs to be larger than buying price.

def besttimetobuyandsell(nums):
    # biggest 2 nums diffs right to left
    pairs = []
    for i, el in enumerate(nums):
        j = i+1
        while j < len(nums):
            pairs.append( (i, j, nums[j]-nums[i]))
            j += 1
    print(pairs)
    return max(pairs, key=lambda t: t[2])


print(besttimetobuyandsell([7,1,5,3,6,4]))