# Given three integer arrays sorted in ascending order, find the smallest number that is common in all three arrays.

def smallestcommon(arr1, arr2, arr3):
    for el1 in arr1:
        for el2 in arr2:
            for el3 in arr3:
                if el1 == el2 == el3:
                    return el1
    return None

# CAN BE IMPORVED BY CHECKING INTERSECTION OF 2 against the 3rd
# CAN BE IMPROVED WITH 1 loop

print(smallestcommon([1,2,3,4,5,6], [0, 2, 3,8, 11], [-3, -2, 1, 2, 5, 19, 21]))