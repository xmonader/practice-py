# Given a sorted array of integers, return the low and high index of the given key. Return -1 if not found. The array length can be in millions with lots of duplicates.

def lowhigh(arr, val):
    i = 0
    low = -1
    for i, item in enumerate(arr):
        if item == val:
            low = i
            break

    high = -1
    j = len(arr)
    while j >= 0:
        if item == arr[j]:
            high = j


    return low, high
