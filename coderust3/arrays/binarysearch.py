
def bsearch(arr, el):
    low = 0
    high = len(arr)

    while low < high:
        mid = low+ (high-low)//2
        midel = arr[mid]
        if midel == el:
            return mid
        elif el > midel:
            low = mid
        else:
            high = mid
    return -1


print(bsearch([3,4,5,7,8, 9, 10], 8))
            