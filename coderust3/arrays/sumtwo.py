# Given an array of integers, return indices of the two numbers such that they add up to a specific target.

# You may assume that each input would have exactly one solution, and you may not use the same element twice.

# Example:

# Given nums = [2, 7, 11, 15], target = 9,

# Because nums[0] + nums[1] = 2 + 7 = 9,
# return [0, 1].


# n^2
# def twosum(nums, target):
#     i = 0 
#     j = 1

#     while i < len(nums):
#         while j < len(nums):
#             if nums[i] + nums[j] == target:
#                 return nums[i], nums[j]
#             j += 1
#         i += 1
#     return None

# res = twosum([0, 1,2,3,4,5,6,7,9,10], 10)
# print(res)
        

# 2N -> N (2pass)
# def twosum(nums, target):
#     tbl = {}
#     for i, n in enumerate(nums):
#         tbl[n] = i
    
#     for n in nums:
#         if target-n in tbl: # has the complement
#             return n, target-n
#             # or return the indices
#     return None
# res = twosum([1,2,3,4,5,6,7,10], 10)
# print(res)


# 2N -> N (1pass)
def twosum(nums, target):
    tbl = {}
    for i, n in enumerate(nums):
        if target-n in tbl: # has the complement
            return n, target-n
        tbl[n] = i
    return None
res = twosum([1,2,3,4,5,6,7,10], 10)
print(res)