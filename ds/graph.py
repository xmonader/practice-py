
from collections import defaultdict
from collections import deque


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.vertices = []

    def addEdge(self, source, dest):
        if source not in self.vertices:
            self.vertices.append(source)
        if dest not in self.vertices:
            self.vertices.append(dest)
        self.graph[source].append(dest)

    def addBiEdge(self, source, dest):
        self.addEdge(source, dest)
        self.addEdge(dest, source)

    def bfs(self, source):
        deq = deque()
        visited = [False] * len(self.vertices)
        print("VISITED: ", visited)
        deq.append(source)
        visited = set()
        
        while deq:
            node = deq.popleft()
            print("Action on node: ", node)
            for nei in self.graph[node]:
                if nei not in visited:
                    deq.append(nei)
                    visited.add(nei)

    def _dfs_fn(self, node, visited):

        visited.add(node)
        print("Action on node: ", node)
        for nei in self.graph[node]:
            if nei not in visited:
                self._dfs_fn(nei, visited)

    def dfs(self, source):
        visited = set()
        self._dfs_fn(source, visited)

    # def _has_cycle(self, source, visited):
    #     visited.add(source)

    #     for nei in self.graph[source]:
    #         if nei in visited:
    #             has_cycle, visited = self._has_cycle(nei, visited)
    #             if has_cycle is True:
    #                 return True, visited
    #     return False, visited

    # def has_cycle(self):
    #     visited = set()
    #     for node, neis in self.graph.items():
    #         hascycle, visited = self._has_cycle(node, visited)
    #         if hascycle:
    #             return True
    #     return False

    def __str__(self):
        return str(self.graph)




    # #Perform DFS Traversal on graph starting from source and 
    # #while traversing if we find a vertex which is already visited
    # #we will know that there is a cycle in the graph
    def has_cycle(self, source=1):
        num_of_vertices = self.vertices
        #A list to hold the history of visited nodes (by default all false)
        #Make a node visited whenever you push it into stack
        visited = set()
        #Create Stack(Implemented in previous lesson) for Depth First Traversal and Push source into the stack
        stack = []
        stack.append(source)
        visited.add(source)
        #Traverse while stack is not empty
        while stack:
            print("LOOP START: STACK NOW {} , visited {}".format(stack, visited))
            #Pop a vertex/node from stack 


            ## STACK IS USED FOR DFS...
            current_node = stack.pop()
            print("   CURRENT NODE: ", current_node)
            #Get adjacent vertices to the current_node from the list,
            #and if anyone of them is already in visited array then cycle exists so 
            #return true
            for nei in self.graph[current_node]:
                print("     NEI:", nei)
                if nei not in visited:
                    stack.append(nei)
                    visited.add(nei)
                else:
                    return True
            print("LOOP DONE: STACK NOW {} , visited {}".format(stack, visited))
        return False

    def num_edges(self):
        total = 0
        for n, nei in self.graph.items():
            for n in nei:
                total += 1
        return total

    def can_reach(self, source, target):
        stk = []
        visited = set()
        stk.append(source)
        visited.add(source)

        while stk:
            node = stk.pop()
            if node == target:
                return True
            for nei in self.graph[node]:
                if nei not in visited:
                    stk.append(nei)
                    visited.add(nei)
        return False
    

    def find_path(self, start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        
        if start not in self.graph:
            return None
        
        for nei in self.graph[start]:
            if nei not in path:
                newpath = self.find_path(nei, end, path)
                if newpath:
                    return newpath
        return None

    def shortest_path(self, source, destination):
        result = 0
        #A list to hold the history of visited nodes (by default all false)
        #Make a node visited whenever you enqueue it into queue
        visited = set()

        #For keeping track of distance of current_node from source
        distance = {}
        queue = deque()
        #Create Queue for Breadth First Traversal and enqueue source in it
        queue.append(source)
        visited.add(source)
        #Traverse while queue is not empty
        while queue:
            #Dequeue a vertex/node from queue and add it to result
            current_node = queue.popleft()
            if current_node not in distance:
                distance[current_node] = -1

            #Get adjacent vertices to the current_node from the list,
            #and if they are not already visited then enqueue them in the Queue
            #and also update their distance from source by adding 1 in current_nodes's distance
            for nei in self.graph[current_node]:
                if nei not in visited:
                    queue.append(nei)
                    visited.add(current_node)
                distance[nei] = distance[current_node] + 1
        #end of while
        return distance[destination]




def main():
    g = Graph()
    g.addEdge(2, 4)
    g.addEdge(2, 5)
    g.addEdge(1, 3)
    g.addEdge(1, 2)
    g.addEdge(3, 1)
    

    g.addEdge(3,6)
    g.addEdge(6,7)
        
    # g.bfs(1)

    # print(g)

    # g.dfs(1)

    print(g.has_cycle())
    print(g.num_edges())

    print(g.can_reach(3, 1))
    print(g.can_reach(1, 5))
    print(g.can_reach(1, 6))
    print(g.shortest_path(1, 3))
    print(g.shortest_path(1, 7))

if __name__ == '__main__':
    main()