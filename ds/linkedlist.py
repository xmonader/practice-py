

class Node:
    def __init__(self, data, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return "Node({}, {})".format(self.data, self.next)

class LinkedList:
    def __init__(self, alist=None):
        self.head = None
        self._llen = 0

        if alist:
            for el in alist:
                self.append(el)

    def prepend(self, data):
        node = Node(data)
        node.next = self.head
        self.head = node
        self._llen += 1

    def append(self, data):
        node = Node(data)
        if self.head:
            n = self.head
            while n.next != None:
                n = n.next
            n.next = node
        else:
            self.head = node
        self._llen += 1

    def len_naive(self):
        n = self.head
        res = 0
        while n:
            res += 1
            n = n.next
        return res

    def length(self):
        return self._llen

    def find(self, data):
        n = self.head
        while n:
            if n.data == data:
                return True
            n = n.next
        return False

    def delete(self, data):

        if self.head.data == data:
            self.head = self.head.next
            self._llen -= 1
        else:
            n = self.head
            while n:
                nnext = n.next
                if nnext and n.next.data == data:
                    nnext = n.next.next
                    n.next = nnext
                    self._llen -= 1
                n = n.next
                    
        return True

    ## REVISE..
    def reverse(self):
        # 1 -> 2 -> 3
        # reverse prev and next elements
        current = self.head
        prev = None
        next = None

        while current:
            # at 2 -> next is 3 should be prev, and prev is 1 should be next
            next = current.next
            current.next = prev
            prev = current
            current = next
    
        self.head=prev

    def hascycle(self):
        visited = set()
        n = self.head
        while n:
            if n in visited:
                return True
            visited.add(n)
            n = n.next
        return False

    def findmid(self):
        slow = fast = self.head
        while fast.next:
            slow = slow.next
            fast = fast.next.next

        return slow

    def __str__(self):
        return str(self.head)

    
    # REVISE
    def remove_duplicates(self):
        prev = self.head
        cur = self.head.next

        visited = set()     
        while cur:
            val = cur.data
            if val in visited:
                prev.next = cur.next
                cur = cur.next
                continue
            visited.add(val)

            prev = cur
            cur = cur.next


# def main():
#     ll = LinkedList([1,2,3,4,5])
#     print(ll)

#     for el in [9,10,11,12]:
#         ll.prepend(el)
#     print(ll)
#     print(ll.find(5))
#     print(ll.len_naive())
#     print(ll.length())
#     print(ll.delete(3))
#     print(ll)
#     print(ll.delete(12))
#     print(ll)
#     print(ll.len_naive())
#     print(ll.length())

#     ll.reverse()
#     print(ll)
#     print(ll.findmid())
# if __name__ == '__main__':
#     main()