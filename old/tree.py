
class Node:
    def __init__(self, val=None, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right



class Tree:
    def __init__(self, val, left=None, right=None):
        self.root = Node(val, left, right)

    def add(v, node=self.root):
        if v > node.val:
            return self.add(v, node.right)
        elif v < node.val:
            return self.add(v, node.left)
        
    def find(v, node=self.root):
        if node is None:
            return False
        if v > node.val:
            return find(v, node.right)
        elif v < node.val:
            return find(v, node.left)
        else:
            return node 
