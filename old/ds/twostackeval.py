import operator

op_map = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.truediv
}

def eval_inp(inp):
    v_stack = []
    op_stack = []
    inp_toks = [x for x in inp.split(" ") if x.strip()]
    for tok in inp_toks:
        if tok.isdigit():
            v_stack.append(float(tok))
        elif tok not in ["(", ")"]:
            # operator
            op_stack.append(tok)
        elif tok == "(":
            continue
        elif tok == ")":
            # eval here
            v1, v2 = v_stack.pop(), v_stack.pop()
            op = op_stack.pop()
            assert op in op_map
            v_stack.append(op_map[op](v1, v2))

    return v_stack.pop()

def test():
    print(eval_inp("( 1 + 2 )"))
    print(eval_inp("( 1 + ( 1 + 2 ) ) "))
    print(eval_inp("( ( 6 * ( 1 + 2 ) ) + ( 3 - 2 ) )"))
test()