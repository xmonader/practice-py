
class Node:
    def __init__(self, val=None, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def __str__(self):
        return "        {}  \n{}            {}\n".format(self.val, self.left, self.right)
    def insert(self, v):
        if v < self.val:
            if self.left is None:
                self.left = Node(v)
            else:
                self.left.insert(v)
        elif v > self.val:
            if self.right is None:
                self.right = Node(v)
            else:
                self.right.insert(v)    

    def find(self, v):
        if v == self.val:
            return self
        
        if v < self.val:
            if self.left:
                return self.left.find(v)
            else:
                return False
        elif v > self.val:
            if self.right:
                return self.right.find(v)
            else:
                return False    
    

class Tree:
    def __init__(self, val, left=None, right=None):
        self.root = Node(val, left, right)

    def invert(self, n):
        if n is None:
            return None
        
        newnode = n
        newnode.left = self.invert(newnode.right)
        newnode.right = self.invert(newnode.left)

        return newnode
