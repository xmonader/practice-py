
def leftchild_idx(pidx):
    return pidx*2 

def rightchild_idx(pidx):
    return pidx*2  + 1

def parentidx(lidx):
    return lidx//2

class Heap:
    def __init__(self):
        self._heap = [0]
        self._cursize = 0

    def findmin(self):
        return self._heap[1]

    def min_child(self, pidx):
        left = self._heap[leftchild_idx(pidx)]
        right = self._heap[rightchild_idx(pidx)]
        if left < right:
            return leftchild_idx(pidx)
        return rightchild_idx(pidx)

    def percdown(self, pidx):
        while leftchild_idx(pidx) < self._cursize:
            minchildidx = self.min_child(pidx)
            if self._heap[pidx] > self._heap[minchildidx]:
                self._heap[pidx], self._heap[minchildidx] = self._heap[minchildidx], self._heap[pidx]
            pidx = minchildidx

    def removemin(self):
        res = self.findmin()
        self._heap[1] = self._heap[self._cursize]
        self._heap.pop() # remove the last one.
        self._cursize -= 1
        self.percdown(1)

        return res

    def add(self, val):
        print("HEAP: ", self._heap)

        print(f"Inserting {val}")

        self._heap.append(val)
        self._cursize += 1
        self.percup(self._cursize)

    def percup(self, i):
        print("Calling percup on I: ", i)

        print("HEAP: ", self._heap)
        pidx = parentidx(i)
        print(" parentidx: ", pidx, " val: ", self._heap[pidx] )
        while pidx > 0:
            if self._heap[i] < self._heap[pidx]:
                print(f"        Child {self._heap[i]} < than parent {self._heap[pidx]} will swap")
                self._heap[i], self._heap[pidx] = self._heap[pidx], self._heap[i]
                print(f"        HEAP NOW: {self._heap}")
            i = pidx
            pidx = pidx//2

    # def percup(self, i):
    #     if i == 0:
    #         return 
    #     pidx = parentidx(i)
    #     if self._heap[i] < self._heap[pidx]:

    #         print(f"percup: {self._heap[i]} {self._heap[pidx]}")
    #         self._heap[i], self._heap[pidx] = self._heap[pidx], self._heap[i]
    #         self.percup(pidx//2)
    # def add(self, val):
    #     self._heap.append(val)
    #     self.heapify(len(self._heap)-1)
    
    # def heapify(self, idx):
    #     if idx%2 == 0:
    #         pidx = parentidx(idx)
    #     else:
    #         pidx = parentidx(idx-1) 
        
    #     el = self._heap[idx]
    #     parent = self._heap[pidx]
    #     while parent < el:
    #         self._heap[pidx], self._heap[idx] = self._heap[idx], self._heap[pidx]
    #         el = self._heap[idx]
    #         parent = self._heap[pidx]

    
    def __str__(self):
        return str(self._heap)

def main():
    h = Heap()
    for el in [1,8,6,3,9,10, 11, 13, 2]:
        h.add(el)
    print(h)

if __name__ == '__main__':
    main()
    
