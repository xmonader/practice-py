
class Trie:
    def __init__(self, *words):
        self._root = {}
        self.init_trie(*words)

    def init_trie(self, *words):
        for w in words:
            cur_dict = self._root
            for c in w:
                cur_dict = cur_dict.setdefault(c, {})
                # l = cur_dict.get(c, None)
                # if l is None:
                #     cur_dict[c] = {}
                #     cur_dict = cur_dict[c]
            cur_dict['__fullword__'] = True
    
    def __contains__(self, key):
        cur_dict = self._root
        for c in key:
            if c not in cur_dict:
                return False
            if c in cur_dict:
                cur_dict = cur_dict[c]
        else:
            if '__fullword__' in cur_dict:
                return True
            return False

    def __str__(self):
        return str(self._root)


if __name__ == "__main__":
    t = Trie('bath', 'bad', 'bed', 'live', 'lives', 'love')
    print(t)
    print('bath' in t)
    print('bed' in t)
    print('baz' in t)
    print('love' in t)
    print('loves' in t)