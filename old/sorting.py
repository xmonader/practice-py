def bubble(lst):
    print("Input: ", lst)
    for pas in range(len(lst)):
        print("PASS: ", pas)
        for left in range(len(lst)-1):
            # u can add len(lst)-pas-1  the elements on the right are already sorted.
            right = left+1
            if lst[left] > lst[right]:
                print(lst[left], lst[right])
                lst[left], lst[right] = lst[right], lst[left]

    return lst

def selection(lst):
    print("Input: ", lst)
    for slot in range(len(lst)):
        print("slot: ", slot)
        smallest = slot
        for i in range(slot, len(lst)):
            if lst[i] < lst[smallest]:
                smallest = i
        print(lst[i], lst[smallest])
        lst[slot], lst[smallest] = lst[smallest], lst[slot]
        print("LST: ", lst)

    return lst

def insertion(lst):
    # FIXME
    # for idx in range(1, len(lst)):
    #     currentval = lst[idx]

    #     left = idx - 1
    #     while left > 0 and lst[left] > currentval:
    #         lst[left+1] = lst[left]
    #         left -= 1
    #     lst[left+1] = currentval


    for i in range(1, len(lst)):
 
        key = lst[i]
 
        # Move elements of lst[0..i-1], that are
        # greater than key, to one position ahead
        # of their current position
        j = i-1
        while j >=0 and key < lst[j] :
                lst[j+1] = lst[j]
                j -= 1
        lst[j+1] = key

    return lst
    

def merge():
    pass

def quick():
    pass


if __name__ == "__main__":
    print(bubble([3,4,5,2,0]))
    print(selection([3,4,2,5,0]))
    print(insertion([3,4,2,5,0]))