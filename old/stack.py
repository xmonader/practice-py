from linkedlist import LinkedList, Node


class Stack:
    def __init__(self):
        self._ll = LinkedList()

    def push(self, val):
        self._ll.prepend(Node(val))

    def pop(self):
        return self._ll.remove_first().val

    def count(self):
        return self._ll.count

    def peek(self):
        return self._ll.head.val

    def __str__(self):
        return str(self._ll)


s = Stack()
s.push(3)
s.push(5)
s.push(0)
print(s)
print(s.pop())
print(s.peek())
s.pop()
print(s)

import operator

op_map = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.truediv
}


def eval_postfix_calc(inp):
    s = Stack()
    for t in [x.strip() for x in inp.split()]:
        if t.isdigit():
            s.push(float(t))
        else:
            right = s.pop()
            left = s.pop()
            s.push(op_map[t](left, right))

    return s.pop()


eval_postfix_calc("3 5 7 * +")

## TODO array based stack