class Node:
    def __init__(self, val, next=None):
        self.val = val
        self.next = next

    def __str__(self):
        return "Node (val= {}, next {})".format(self.val, self.next)

def printnodes(n):
    while n != None:
        print("N: ", n)
        n = n.next

n1 = Node(3)
n2 = Node(5)
n3 = Node(8)


# 3 -> 5 - > 8
n1.next = n2
n2.next = n3
printnodes(n1)


class LinkedList:
    def __init__(self):
        self.head = None 
        self.tail = None
        self.count = 0

    def prepend(self, n):
        oldhead = self.head
        self.head = n
        self.head.next = oldhead
        self.count += 1

        if self.count == 1:
            self.tail = self.head
        
    def append(self, n):
        if self.count == 0:
            self.head = n
            self.tail = n
        else:
            self.tail.next = n
            self.tail = n
        
        self.count += 1
    
    def __str__(self):
        return "<LinkedList len={} chain={} >".format(self.count, str(self.head))

    def remove_first(self):
        n = None
        if self.count > 0:
            n = self.head
            self.head = self.head.next
            self.count -= 1
            # if list of 1 element
            self.tail = self.head
        return n


    def remove_last(self):
        pass

    def find(self, n):
        pass

    def remove(self, n):
        current = self.head
        prev = None

        while current != None:
            if current.val == n.val:
                if prev != None:
                    prev.next = current.next
                    self.count -= 1
                    if self.count == 0:
                        self.tail = prev
                else:
                    self.remove_first()
            prev = current
            current = current.next

    def reset(self):
        self.head = self.tail = None
        self.count = 0
    def __iter__(self):
        n = self.head
        while n != None:
            yield n
            n = n.next


ll = LinkedList()
print(ll)

ll.append(Node(3))
ll.append(Node(56))
ll.append(Node(99))

print(ll)
ll.prepend(Node(0))
print(ll)

ll.prepend(Node(36))
print(ll)

ll.remove_first()
print(ll)

for n in ll:
    print(n)

ll.remove(Node(56))
print(ll)