from collections import defaultdict

def mk_badmatch_table(pat):
    d = defaultdict(lambda : len(pat))
    for i, c in enumerate(pat[:-1]): # except for the last char
        d[c] = len(pat) - i - 1
    return d

def boyermoore(pat, s):
    bad = mk_badmatch_table(pat)
    startidx = 0
    while startidx < len(s) - len(pat):
        ncharstomatch = len(pat) - 1
        while ncharstomatch > 0 and pat[ncharstomatch] == s[startidx+ncharstomatch]:
            ncharstomatch -= 1
        if ncharstomatch == 0:
            return startidx
        else:
            # print("updating startid to: ", bad[s[startidx + len(pat)-1]])
            startidx += bad[s[startidx + len(pat)-1]]



bad = mk_badmatch_table("truth")
print(bad)

k = "truth"
sent =  " i hate turtles but i truces with truth are the best"
print(boyermoore(k, sent))
print(sent.index(k))