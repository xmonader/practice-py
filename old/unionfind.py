class UF:
    def __init__(self):
        self._tbl = {}

    def is_connected(self, first, second):
        if first in self._tbl and second in self._tbl:
            return self._tbl[first] == self._tbl[second]
        return False

    def connect(self, first, second):
        if first not in self._tbl:
            self._tbl[first] = first
        first_val = self._tbl[first]
        second_val = self._tbl.get(second, None)
        if second_val is None:
            second_val = second
            self._tbl[second] = second
        
        for k, v in self._tbl.items():
            if v == second_val:
                self._tbl[k] = first_val

    def __str__(self):
        return str(self._tbl)

class QUF:
    def __init__(self, count=50):
        self._arr = [n for n in range(count)]

    def root(self, n):
        while n != self._arr[n]:
            n = self._arr[n]
        return n

    def is_connected(self, first, second):
        return self.root(first) == self.root(second)

    def connect(self, first, second):
        i = self.root(first)
        j = self.root(second)
        self._arr[i] = j

    def __str__(self):
        return str(self._arr)

def test():
    uf = QUF()
    print(uf.is_connected(3,4))
    uf.connect(0, 3)
    print(uf.is_connected(0, 3))
    uf.connect(3, 4)
    print(uf.is_connected(3, 4))
    print(uf.is_connected(0, 4))
    uf.connect(7, 8)
    uf.connect(8, 33)
    uf.connect(3, 7)
    print(uf.is_connected(0, 33))
    print(uf)
test()