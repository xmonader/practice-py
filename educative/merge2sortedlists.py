#Merge arr1 and arr2 and return resulted array
def mergeSortedLists(arr1,arr2):
    res = [-1]*(len(arr1)+len(arr2))
    l1i = 0
    l2i = 0
    resi = 0
    while l1i < len(arr1) and l2i < len(arr2):
        if arr1[l1i] < arr2[l2i]:
            res[resi] = (arr1[l1i])
            l1i += 1
            resi += 1
        if arr2[l2i] < arr1[l1i]:
            res[resi] = arr2[l2i]
            l2i += 1
            resi += 1

    while l1i < len(arr1):
        res[resi] = arr1[l1i]

        l1i += 1
        resi += 1

    while l2i < len(arr2):
        res[resi] = (arr1[l2i])

        l2i += 1
        resi += 1
    return res

        
print(mergeSortedLists([1,2,5,8], [0, 3,4,6,7]))