from collections import deque

def find_bins(n):
    q = deque()
    q.append(1)

    res = []
    for i in range(n):
        res.append(str(q.popleft()))
        with0 = res[i] + "0"
        with1 = res[i] + "1"
        q.append(int(with0))
        q.append(int(with1))

    return res


def main():
    print(find_bins(5))

if __name__ == '__main__':
    main()