from collections import deque

#1. Use a second tempStack.
#2. Pop value from mainStack.
#3. If the value is greater or equal to the top of tempStack, then push the value in tempStack
#else pop all values from tempStack and push them in mainStack and in the end push value in tempStack and repeat from step 2.
#till mainStack is not empty.
#4. When mainStack will be empty, tempStack will have sorted values in descending order.
#5. Now transfer values from tempStack to mainStack to make values sorted in ascending order.
def sortStack(stack,size):

		tempStack = deque(maxlen=size)

		while stack.isEmpty() == False:

			value = stack.pop()

			if value >= tempStack[-1]:
				tempStack.append(value)
			else:
				while tempStack:
					stack.append(tempStack.pop())
				tempStack.append(value)
    
		#Transfer from tempStack => stack
		while tempStack:
			stack.append(tempStack.pop())