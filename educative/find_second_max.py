

def find_2ndmax(alist):
    i = 0
    maxest = float("-inf")
    secondmaxest = float("-inf")

    for i, el in enumerate(alist):
        if maxest < el:
            secondmaxest = maxest
            maxest = el
    return secondmaxest


print(find_2ndmax([3,4,5,9,2,11]))
