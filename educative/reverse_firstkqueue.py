from collections import deque

def reverseK(q, k):
    stack = deque()
    for i in range(k):
        stack.append(q.popleft())

    while stack:
        q.append(stack.pop())
    
    for i in range(len(q) - k):
        q.append(q.popleft())


    return q

def main():
    q = deque([1,2,3,4,5,6,7,8,9,10, 122, 41])
    print(reverseK(q, 5))

if __name__ == '__main__':
    main()