def findSum(arr,value):
    # Write - Your - Code
    i = 0

    while i <len(arr):
        j = 0
        while j < len(arr):
            if arr[i]+arr[j] == value:
                yield arr[i], arr[j]
            j += 1
        i += 1
    
    return arr


print(list(findSum([3,4,5,2,1,4], 3)))


def findSum2(arr,value):
    # Write - Your - Code
    tbl = {} # u can use sets. o(1) search..

    for el in arr:
        if value - el in tbl:
            yield el, value-el
        else:
            tbl[el] = True
    return arr
print(list(findSum2([3,4,5,2,1,4], 3)))

def findSum3(arr,value):
    # Write - Your - Code
    tbl = set() # u can use sets. o(1) search..
    
    for el in arr:
        if value - el in tbl:
            yield el, value-el
        else:
            tbl.add(el)
    return arr

print(list(findSum3([3,4,5,2,1,4], 3)))