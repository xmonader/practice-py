def findmin(alist):
    _min = alist[0]
    for el in alist[1:]:
        if el < _min:
            _min = el
    return _min


print(findmin([-1, -2, 5, 9]))