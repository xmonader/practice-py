

def first_unique(alist):
    i = 0
    while i < len(alist):
        j = 0        
        repeated = False
        while j < len(alist):
            if alist[j] == alist[i] and i!=j:
                repeated = True
                break
            j += 1
        
        if repeated is False:
            return alist[i]
        i += 1
    
    return -1

print(first_unique([1,2,3,4,2,1]))
print(first_unique([2,2,3,3,4,4]))