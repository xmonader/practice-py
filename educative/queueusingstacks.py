class Q:

    def __init__(self, els):
        self.stk = []
        self.helper = []
        for el in els:
            self.enq(el)

    def enq(self, el):
        while len(self.stk):
            self.helper.append(self.stk.pop())

        self.stk.append(el)
        while len(self.helper):
            self.stk.append(self.helper.pop())
# 5
# 4
# 3
 

#  3
#  4
#  5
    def deq(self):
        return self.stk.pop()


    def __str__(self):
        return str(self.stk)
def main():
    
    q = Q([1])
    q.enq(3)
    q.enq(5)
    print(q)

    q.deq()
    print(q)
    q.deq()
    print(q)

if __name__ == '__main__':
    main()
