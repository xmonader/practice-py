


def is_power_of_two1(n):
    import math
    return math.log2(n).is_integer()
    # 2 ^ 3 = 8

    # log2 8 = 3 

def is_power_of_two2(n):
    while n > 0:
        if n%2 == 0:
            n /= 2
    return n%2 == 0

def main():
    print(is_power_of_two1(8))
    print(is_power_of_two1(7))

    print(is_power_of_two2(8))
    print(is_power_of_two2(7))
if __name__ == '__main__':
    main()
