import pythonds as ds
import string

import random

bst = ds.trees.BinarySearchTree()
bst.put(10,10)
bst.put(15,15)
bst.put(12,12)
bst.put(11,11)
bst.put(13,13)


def height(root):
    # import ipdb; ipdb.set_tqrace()
    return 1+max(height(root.leftChild), height(root.rightChild))

print(height(bst.root))