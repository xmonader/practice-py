# Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

# Example 1:

# Input: "babad"
# Output: "bab"
# Note: "aba" is also a valid answer.
# Example 2:

# Input: "cbbd"
# Output: "bb"

def longestpalindrome(s):
    i = 0
    palins = []
    while i < len(s):
        c = s[i]
        c_ridx = -1
        try:
            c_ridx = s.rindex(c, i)
        except:
            pass
        
        if c_ridx != -1:
            palin = ""
            j = c_ridx
            icheck = i
            while icheck<len(s) and s[icheck] == s[j] and i<=j:
                # print("S[%d] "%icheck, s[icheck], "s[%d]:"%j, s[j])
                palin += s[j] 
                j -= 1
                icheck += 1
            if len(palin) > 1:
                palins.append(palin)
        i += 1
    return sorted(palins, key=len)[-1]
    # return palins

tests = ["babad", "cbbd", "racecar ok" ]
for t in tests:
    print(longestpalindrome(t))