
# For new programmer, this is very essential... We know a little bit for that we don't answer many problem as like "largest sum contiguous sub array" that's really very easy to determine largest sum if we know the following algorithm.....

# Initialize: max_so_far = 0

# max_ending_here = 0
# Loop for each element of the array

# (a) max_ending_here = max_ending_here + a[i]

# (b) if(max_ending_here < 0) max_ending_here = 0

# (c) if(max_so_far < max_ending_here) max_so_far = max_ending_here

# return max_so_far

# Explanation: Simple idea of the Kadane's algorithm is to look for all positive contiguous segments of the array (max_ending_here is used for this). And keep track of maximum sum contiguous segment among all positive segments (max_so_far is used for this). Each time we get a positive sum compare it with max_so_far and update max_so_far if it is greater than max_so_far

# Lets take the example:

# {-2, -3, 4, -1, -2, 1, 5, -3}

# max_so_far = max_ending_here = 0

# for i=0, a[0] = -2 max_ending_here = max_ending_here + (-2) Set max_ending_here = 0 because max_ending_here < 0

# for i=1, a[1] = -3 max_ending_here = max_ending_here + (-3) Set max_ending_here = 0 because max_ending_here < 0

# for i=2, a[2] = 4 max_ending_here = max_ending_here + (4) max_ending_here = 4 max_so_far is updated to 4 because max_ending_here greater than max_so_far which was 0 till now

# for i=3, a[3] = -1 max_ending_here = max_ending_here + (-1) max_ending_here = 3

# for i=4, a[4] = -2 max_ending_here = max_ending_here + (-2) max_ending_here = 1

# for i=5, a[5] = 1 max_ending_here = max_ending_here + (1) max_ending_here = 2

# for i=6, a[6] = 5 max_ending_here = max_ending_here + (5) max_ending_here = 7 max_so_far is updated to 7 because max_ending_here is greater than max_so_far

# for i=7, a[7] = -3 max_ending_here = max_ending_here + (-3) max_ending_here = 4

# Code :

# for(int i = 0; i < N; i++){

# max_ending_here += Number[i];

#         if(max_ending_here > max_so_far)max_so_far = max_ending_here;

#         if(max_ending_here < 0)max_ending_here = 0;

#     }

# https://leetcode.com/problems/maximum-subarray/discuss/20193/DP-solution-and-some-thoughts


# Analysis of this problem:
# Apparently, this is a optimization problem, which can be usually solved by DP. So when it comes to DP, the first thing for us to figure out is the format of the sub problem(or the state of each sub problem). The format of the sub problem can be helpful when we are trying to come up with the recursive relation.

# At first, I think the sub problem should look like: maxSubArray(int A[], int i, int j), which means the maxSubArray for A[i: j]. In this way, our goal is to figure out what maxSubArray(A, 0, A.length - 1) is. However, if we define the format of the sub problem in this way, it's hard to find the connection from the sub problem to the original problem(at least for me). In other words, I can't find a way to divided the original problem into the sub problems and use the solutions of the sub problems to somehow create the solution of the original one.

# So I change the format of the sub problem into something like: maxSubArray(int A[], int i), which means the maxSubArray for A[0:i ] which must has A[i] as the end element. Note that now the sub problem's format is less flexible and less powerful than the previous one because there's a limitation that A[i] should be contained in that sequence and we have to keep track of each solution of the sub problem to update the global optimal value. However, now the connect between the sub problem & the original one becomes clearer:

# maxSubArray(A, i) = maxSubArray(A, i - 1) > 0 ? maxSubArray(A, i - 1) : 0 + A[i]; 
# And here's the code

# public int maxSubArray(int[] A) {
#         int n = A.length;
#         int[] dp = new int[n];//dp[i] means the maximum subarray ending with A[i];
#         dp[0] = A[0];
#         int max = dp[0];
        
#         for(int i = 1; i < n; i++){
#             dp[i] = A[i] + (dp[i - 1] > 0 ? dp[i - 1] : 0);
#             max = Math.max(max, dp[i]);
#         }
        
#         return max;
# }



def maxSubArray(A):
    if not A:
        return 0

    curSum = maxSum = A[0]
    for num in A[1:]:
        print(" > NUM {}, curSum {}, maxSum {}".format(num, curSum, maxSum))
        curSum = max(num, curSum + num)
        maxSum = max(maxSum, curSum)
        print("     >>NUM {}, curSum {}, maxSum {}".format(num, curSum, maxSum))

    return maxSum


maxSubArray([-1, -3, 4, 5, -2, -1, 4, -2])