prices = {0:0, 1:1 , 2:5, 3:8, 4:9, 5:10, 6:17, 7:17, 8:20}
# rod cut -> dollar


# price[] as prices of different pieces


# def maxprofitfor(rodlength):
#     # C(i) = max(Vk + C(i-k)); 1 < k < i
#     C = [0 for x in range(len(prices))]
#     C[1] = prices[1] # C(1) = Vk
#     print("C: ", C)
#     for i in range(1, len(C)):
#         C[i] = max((prices[k] + C[i-k-1]) for k in range(i))
#         print(C)
#     return C[rodlength]


# print(maxprofitfor(3))

def rodcut(rodlength, prices={1:1, 2:3, 3:5, 4:10, 5:12}):
    if rodlength == 0:
        return []
    res = []
    for i in range(rodlength+1):
        if i in prices:
            res.extend([i] + [(rodcut(rodlength-i, prices))])
    return res

print(rodcut(2))