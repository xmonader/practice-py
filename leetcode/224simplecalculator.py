
opmap = {
    '+': lambda a, b: a+b,
    '-': lambda a, b: a-b,
}

def eval_exp(s):
    toks = [w for w in s.split() if w.strip() ]
    vals = []
    ops = []

    for t in toks:
        if t.isdigit():
            vals.append(float(t))
        
        elif t in "(+-":
            ops.append(t)
        
        elif t == ")":
            op = ops.pop()
            val1, val2 = vals.pop(), vals.pop()
            vals.append(opmap[op](val1, val2))

    while ops:
        op = ops.pop()
        if op == "(":
            continue
        val1, val2 = vals.pop(), vals.pop()
        vals.append(opmap[op](val1, val2))
        print(vals, " ops: ", ops)        
    # while ops and vals:


    return vals.pop()



def main():
    t = eval_exp("( ( 1 + 2 ) + 4 )")
    print(t)
    t = eval_exp("1 + 2")
    print(t)

if __name__ == '__main__':
    main()    