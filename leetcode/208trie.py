
class Trie:


    def __init__(self, *words):
        self.root = {}
        self._init_words(*words)

    def _init_words(self, *words):
        for word in words:
            self.add(word)

    def add(self, word):
        cur_dict = self.root
        for c in word:
            cur_dict = cur_dict.setdefault(c, {})
        cur_dict['__fullword__'] = True
    
    
    def __contains__(self, word):
        cur_dict = self.root
        for c in word:
            cur_dict = cur_dict.get(c, None)
            if not cur_dict:
                return False
        return cur_dict.get('__fullword__', False)

def main():
    t = Trie("bath", "bad", "man")
    print(t.root)
    print('love' in t)
    print("bath" in t)

if __name__ == '__main__':
    main()
    