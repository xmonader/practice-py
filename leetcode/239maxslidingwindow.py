
def maxslidingwindow(arr, windowsize=2):
    maxs = []
    for i, _ in enumerate(arr):
        if i + windowsize - 1 < len(arr):
            maxs.append(max(arr[i:i+windowsize]))
    return maxs


print(maxslidingwindow([1,3,-1,-3,5,3,6,7], 3))