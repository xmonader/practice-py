def wordbreak(phrase, words_list, res=None):
    if res is None:
        res = []
    
    for w in words_list:
        if phrase.startswith(w):
            res.append(w)

            return wordbreak(phrase[len(w):], words_list, res)

    return res


print("iloverain", ["i", "love", "rain"])