# Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

# An input string is valid if:

# Open brackets must be closed by the same type of brackets.
# Open brackets must be closed in the correct order.
# Note that an empty string is also considered valid.

# Example 1:

# Input: "()"
# Output: true
# Example 2:

# Input: "()[]{}"
# Output: true
# Example 3:

# Input: "(]"
# Output: false
# Example 4:

# Input: "([)]"
# Output: false
# Example 5:

# Input: "{[]}"
# Output: true


def valid_parens(inp):
    stk = []
    opens = "{(["
    parens = [( "(",")" ), ("[", "]"), ("{","}")]
    for c in inp:
        if c in opens:
            stk.append(c)
        if c not in opens:
            if (stk[-1], c) in parens:
                stk.pop()
    
    if stk == []:
        # balanced.
        return True
    return False



def main():
    # Example 1:
    print(valid_parens("()"))
    print(valid_parens("()[]{}"))
    print(valid_parens("(]"))
    print(valid_parens("([)]"))
    print(valid_parens("{[]}"))
    
    # Input: "()"
    # Output: true
    # Example 2:

    # Input: "()[]{}"
    # Output: true
    # Example 3:

    # Input: "(]"
    # Output: false
    # Example 4:

    # Input: "([)]"
    # Output: false
    # Example 5:

    # Input: "{[]}"
    # Output: true

if __name__ == '__main__':
    main()