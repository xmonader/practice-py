# def minchange(value, coins=[0.25,0.5]):
#     if value == 0:
#         return [0]
#     if value < 0:
#         return [0]
    
#     res = []
#     for c in coins:
#         if c == value:
#             res.append(c)
#         if c < value:
#             res.append([c]+ minchange(value-c, coins))
        
#     return res

# print(minchange(0.75))


def minchange(value, coins=[0.25,0.5]):
    if value == 0:
        return 0
    if value < 0:
        return 0

    res = 10000000000
    for c in coins:
        if c == value:
            return 1
        if c < value:
            subres = minchange(value-c, coins)
            if subres + 1 < res:
                res = subres + 1
    return res
print(minchange(.75))
