
class CircQueue:

    def __init__(self, cap):
        self._arr = []
        self.front = 0
        self.back = 0

    def enqueue(self, val):
        self._arr.append(val)
        self.back = ( self.back+1 ) % len(self._arr)
        print(self)

    def dequeue(self):

        el = self._arr[self.front]
        self.front = (self.front+1) % len(self._arr)
        print(self)
        return el

    def __str__(self):
        return f"{self._arr}, front: {self.front}, back: {self.back}"

def main():
    myq = CircQueue(5)
    for i in range(4):
        myq.enqueue(i)
    import ipdb; ipdb.set_trace()    

if __name__ == '__main__':
    main()