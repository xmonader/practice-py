
# Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei), find the minimum number of conference rooms required.

# Example 1:

# Input: [[0, 30],[5, 10],[15, 20]]
# Output: 2
# Example 2:

# Input: [[7,10],[2,4]]
# Output: 1



 # Very similar with what we do in real life. Whenever you want to start a meeting, 
 # you go and check if any empty room available (available > 0) and
 # if so take one of them ( available -=1 ). Otherwise,
 # you need to find a new room someplace else ( numRooms += 1 ).  
 # After you finish the meeting, the room becomes available again ( available += 1 ).
 
#  def minMeetingRooms(self, intervals):
#         starts = []
#         ends = []
#         for i in intervals:
#             starts.append(i.start)
#             ends.append(i.end)
        
#         starts.sort()
#         ends.sort()
#         s = e = 0
#         numRooms = available = 0
#         while s < len(starts):
#             if starts[s] < ends[e]:
#                 if available == 0:
#                     numRooms += 1
#                 else:
#                     available -= 1
                    
#                 s += 1
#             else:
#                 available += 1
#                 e += 1
        
#         return numRooms


## NEED TO VISUALIZE.. TODO:

def meetingroomsneeded(arr):
    starts = []
    ends = []
    for s, e in arr:
        starts.append(s)
        ends.append(e)
    
    starts.sort()
    ends.sort()

    # 0   5 15
    # 10 20 30
    s = e = 0
    roomsavailable = 0
    numrooms = 0
    while s < len(starts):
        start = starts[s]
        end = ends[e]
        if start < end:
            if roomsavailable == 0:
                numrooms += 1
            else:
                roomsavailable -= 1
            s += 1
        else:
            roomavailable += 1
            e += 1


def main():
    print(meetingroomsneeded([[0, 30],[5, 10],[15, 20]]))
    print(meetingroomsneeded([[0, 30],[5, 10],[15, 20]]))

if __name__ == '__main__':
    main()