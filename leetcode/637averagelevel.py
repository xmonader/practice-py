

# FIXME:...
def avg_level(root):
    queue = [(root, 0)]
    levelsavg = []
    while queue:
        size = len(queue)
        tmpres = 0
        for i in range(size):
            el = queue.pop(0)
            if el.left:
                queue.append(el.left)
            if el.right:
                queue.append(el.right)

            tmpres += el.val
        levelsavg.append(tmpres/size)
    return levelsavg    


        