def reverse_str_rec(astring):
    if len(astring) == 0:
        return ""
    return astring[-1] + reverse_str_rec(astring[0:-1])
     

print(reverse_str_rec("hello world"))