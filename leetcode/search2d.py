


def bsearch(m, key):
    nrows= len(m)
    ncols = len(m[0])
    def indexfromrowcol(row, col):
        return row*ncols + col

    def rowcolfromindex(idx):
        return idx//ncols, idx%ncols

    low = indexfromrowcol(0, 0)
    high = indexfromrowcol(nrows, ncols) - 1
    while low <= high:
        mid = (low + high) // 2
        r, c = rowcolfromindex(mid)
        if r>nrows or c > ncols:
            return -1

        print(f"searching for {key} , low: {low} high: {high}, mid: {mid}, maps to {r}, {c} => {m[r][c]}")

        valatmid = m[r][c]
        print(f"VAL AT MID {valatmid}")
        if valatmid == key:
            return r,c
        elif valatmid < key:
            low = mid+1
        else:
            high = mid-1   
    return -1
            

def main():
    matrix = [
                [1,   3,  5,  7],
                [10, 11, 16, 20],
                [23, 30, 34, 50]
             ]

    print(bsearch(matrix, 16))
    print(bsearch(matrix, 50))
    print(bsearch(matrix, 90))

if __name__ == '__main__':
    main()