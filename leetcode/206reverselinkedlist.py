from ds.linkedlist import LinkedList, Node

n1 = Node(1)
n2 = Node(2)
n3 = Node(3)
n4 = Node(4)

n1.next = n2
n2.next = n3
n3.next = n4 
# n4.next = n2


def reverse_linked_list(head):
    cur = head
    prev = None
    while cur:
        next = cur.next
        cur.next = prev
        prev = cur 
        cur = next
    return prev

    