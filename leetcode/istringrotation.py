def is_substring(a, b):
    print(f"A :{a} , B: {b}")
    return a in b


def is_string_rotation(s2, s1):
    return is_substring(s2, s1+s1)

def main():
    s1 = "helloworld"
    s2 = "worldhello"

    print(is_string_rotation(s2, s1))

if __name__ == '__main__':
    main()