
class MinStack:
    def __init__(self):
        self._stk = []
        self._minstk = []

    def append(self, v):
        self._stk.append(v)
        if not self._minstk:
            self._minstk.append(v)
        else:
            if v < self._minstk[-1]:
                self._minstk.append(v)

    push = append
    def pop(self):
        res = self._stk.pop()
        if res == self._minstk[-1]:
            self._minstk.pop()

        return res
    
    def getMin(self):
        print(f"minstk: {self._minstk} ")
        return self._minstk[-1]

def main():
    minStack = MinStack()
    minStack.push(-2)
    minStack.push(0)
    minStack.push(-3)
    minStack.getMin()
    minStack.pop()
    minStack.getMin()

if __name__ == '__main__':
    main()

