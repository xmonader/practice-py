def movezeros(nums):
    #Input: [0,1,0,3,12]
    #Output: [1,3,12,0,0]
    
    i = 0 
    while i < len(nums):
        cur = nums[i]
        if cur == 0:
            nextnonzero = None
            j = i+1
            while j < len(nums):
                if nums[j] != 0:
                    nextnonzero = j
                    break
                j += 1
            if nextnonzero is not None:
                nums[i], nums[nextnonzero] = nums[nextnonzero], nums[i]
        
        i += 1

    return nums


print(movezeros([0,1,0,3,12]))