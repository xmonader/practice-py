def plus1(inp):
    rev = reversed(inp)
    result = []

    initial = 1
    carry = 0
    for n in rev:
        res = n + initial
        initial = 0
        if carry:
            res += 1
            carry = 0
        if res>9:
            carry = 1
            res = res%10
        
        result.append(res)
    if carry:
        result.append(carry)

    return result[::-1]



def main():
    inp = [1,2,3]
    res = [1,2,4]
    print(plus1(inp))
    inp = [9, 9, 9]
    print(plus1(inp))
    # res = [1, 0, 0, 0]

if __name__ == '__main__':
    main()