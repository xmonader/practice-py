def maxDepth(root):
    if root is None:
        return 0
    else:
        i = maxDepth(root.left)
        j = maxDepth(root.right)
        return  j + 1 if (i < j) else i + 1