# Input: [7,1,5,3,6,4]
# Output: 5
# Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
#              Not 7-1 = 6, as selling price needs to be larger than buying price.

def besttimetobuyandsell(nums):
    # biggest 2 nums diffs right to left
    j = len(nums) - 1
    pairs = []
    maxnum_at = ((-1, -1), -1)
    while j >= 1:
        cur_right = nums[j]
        i = 0
        while i < j:
            cur_left = nums[i]
            pairs.append( ((cur_left, cur_right), cur_right-cur_left) ) 
        
            i += 1
        j -= 1

    return max(pairs, key= lambda t: t[1])


print(besttimetobuyandsell([7,1,5,3,6,4]))