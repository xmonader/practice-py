
def myatoi(s):
    res = 0
    # trim spaces.
    s = s.lstrip()
    stack = []
    for c in s:
        if c in ["+", "-"]:
            stack.append(c)
        if c.isdigit():
            stack.append(ord(c) - ord('0'))

    tenthpower = 0
    res = 0
    while tenthpower <= len(stack):
        popped = stack.pop()
        if isinstance(popped, int):
            res += 10**tenthpower * popped
            tenthpower += 1
        else:
            if popped == "-":
                res = -res
            break
    return res


assert myatoi("3") == 3
assert myatoi("50") == 50

