def staircase(n):
    if n == 0:
        return []

    res = []
    for i in range(1, n+1):
        res.extend([ [i]+staircase(n-i)] )

    return res


for n in range(5):
    print("N: ", n, staircase(n))