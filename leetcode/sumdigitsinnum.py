def sumdigits(num):
    if num < 10:
        return num
    return num%10 + sumdigits(num//10)


print(sumdigits(123))