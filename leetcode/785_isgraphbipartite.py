def isgraphbipartite(g):
    colors = {}
    for n in g.graph.keys():
        if n not in colors:
            stack = [n]
            for nei in g.graph[n]:
                if nei not in colors:
                    stack.append(nei)
                    colors[nei] = colors[n] ^ 1
                else:
                    if colors[nei] == colors[n]:
                        return False
    return True

