def island_perimeter(grid):
    N, M = len(grid), len(grid[0])
    def land_neigbors(row, col):
        # we are starting from above and left so we can ignore the neigbor from above and the left..
        # n1 = grid[row-1][col]
        nd = nr = 0
        if row+1 < N:
            nd = grid[row+1][col]
        if col+1 < M:
            nr = grid[row][col+1]

        return nd+nr

        
    
    # total_perm = land*3 - neigbors..
    neigbors = 0
    lands = 0
    for i, row in enumerate(grid):
        for j, col in enumerate(row):
            if grid[i][j] == 1:
                lands += 1

                neigbors += land_neigbors(i, j)

    return lands*4 - neigbors*2
            


def main():
    land = [[0,1,0,0],
            [1,1,1,0],
            [0,1,0,0],
            [1,1,0,0]]
    print(island_perimeter(land))

if __name__ == '__main__':
    main()

