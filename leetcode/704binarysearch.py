def bsearch(nums, target):
    lidx, hidx = 0, len(nums)-1
    while lidx < hidx:
        midx = lidx + hidx //2
        if target > nums[midx]:
            lidx = midx
        elif target < nums[midx]:
            hidx = midx
        elif target == nums[midx]:
            # target = midx
            return midx
        
    return False


print(bsearch([3,4,5,6], 11))
