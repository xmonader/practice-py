# Given a linked list, determine if it has a cycle in it.

# Follow up:
# Can you solve it without using extra space?


from ds.linkedlist import LinkedList, Node

def hascycle(head):
    fast = slow = head
    while fast and fast.next:
        slow = slow.next
        fast = fast.next.next
        if fast == slow:
            return True
    return False


ll = LinkedList()

n1 = Node(1)
n2 = Node(2)
n3 = Node(3)
n4 = Node(4)

n1.next = n2
n2.next = n3
n3.next = n4 
# n4.next = n2

print(hascycle(n1))


