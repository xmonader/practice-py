def compress_str(s):
    res = ""
    i = 0
    while i < len(s):
        c = s[i]
        j = i+1
        while j < len(s) and s[j] == c:
            j += 1
        if j-i > 1:
            res += c + str(j-i)
        else:
            res += c
        i = j
    return res

print(compress_str("aaabcccddddddddefff"))