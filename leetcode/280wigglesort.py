
def wigglesort(arr):
    arr.sort()
    for i in range(1, len(arr)-1, 2):
        arr[i], arr[i+1] = arr[i+1], arr[i]
    return arr

print(wigglesort([12,0,2,3,5,9,23]))