def judgecircle(moves):
    # LDUR
    x = y = 0
    for m in moves:
        if m == "L":
            x -= 1
        elif m == "R":
            x += 1
        elif m == "U":
            y -= 1
        elif m == "D":
            y += 1
    return x == y == 0


print(judgecircle('UDLL'))
print(judgecircle('UDLR'))
