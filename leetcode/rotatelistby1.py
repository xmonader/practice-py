def rotate_list1(alist):
    # [3,4,5,2] => [2,3,4,5]
    res = [-1]*len(alist)
    res[0] = alist[-1]
    res[1:] = alist[:-1]

    return res


print(rotate_list1([3,4,5,2]))