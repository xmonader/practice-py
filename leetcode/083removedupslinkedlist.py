from ds.linkedlist import LinkedList, Node

n1 = Node(1)
n2 = Node(2)
n3 = Node(2)
n4 = Node(4)

n1.next = n2
n2.next = n3
n3.next = n4 


# n4.next = n2

def remove_dups(head):
    current = head
    while current and current.next:
        if current.val == current.next.val:
            current.next = current.next.next
        else:
            current = current.next
    return head

print(remove_dups(n1))