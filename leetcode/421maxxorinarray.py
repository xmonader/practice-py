from operator import xor

def max_xor(arr):
    pairs = []
    for i, el1 in enumerate(arr):
        for j, el2 in enumerate(arr[1:]):
            pairs.append( (el1, el2, xor(el1, el2)) )

    return max(pairs, key=lambda t: t[2])


# O(n) solution

# def findMaximumXOR(self, nums):
#     answer = 0
#     for i in range(32)[::-1]:
#         answer <<= 1
#         prefixes = {num >> i for num in nums}
#         answer += any(answer^1 ^ p in prefixes for p in prefixes)
#     return answer
# Build the answer bit by bit from left to right. 
# Let's say we already know the largest first seven bits we can create.
#  How to find the largest first eight bits we can create? 
# Well it's that maximal seven-bits prefix followed by 0 or 1. Append 0 and then try to create the 1 one
#  (i.e., answer ^ 1) from two eight-bits prefixes from nums. If we can, then change that 0 to 1.

def main():
    arr =[3, 10, 5, 25, 2, 8]
    print(max_xor(arr))

if __name__ == '__main__':
    main()