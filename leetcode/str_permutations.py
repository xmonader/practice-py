def str_permutations(astring):
    if astring == "":
        return []
    if len(astring) == 1:
        return [astring[0]]

    for i, c in enumerate(astring):
        return [ [c] + str_permutations(astring[:i]+astring[i+1:]) ] + [str_permutations(astring[:i]+astring[i+1:]) + [c]]    

print(str_permutations("abc"))