def printstk(stk, stkname="stack"):
    print("STACK: ", stkname)
    for el in stk[::-1]:
        print("------------")
        print(el)


    print("==============")

def sortstack(stk):

    tmpstack = []

    while stk:
        # printstk(stk, "Input")
        print(f"INPUT STACK: {stk}")
        el = stk.pop()
        print(f"Popped EL: {el} ")
        while tmpstack and tmpstack[-1] > el:
            print(f"    {tmpstack} tmpstack top {tmpstack[-1]} > stack el { el} ")
            # printstk(tmpstack, "tmpstk")
            stk.append(tmpstack.pop())
            print(f"    NOW AFTER APPENDING input stack is {stk} and tmpstack {tmpstack} ")

        tmpstack.append(el)
        print(f"tmpstk: {tmpstack}")
    return tmpstack


print(sortstack([5, 3, 9, 6]))