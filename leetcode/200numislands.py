# Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.

# Example 1:

# Input:
# 11110
# 11010
# 11000
# 00000

# Output: 1
# Example 2:

# Input:
# 11000
# 11000
# 00100
# 00011

# Output: 3


class QUF:
    def __init__(self, count=50):
        self._arr = [n for n in range(count)]

    def root(self, n):
        while n != self._arr[n]:
            n = self._arr[n]
        return n

    def is_connected(self, first, second):
        return self.root(first) == self.root(second)

    def connect(self, first, second):
        i = self.root(first)
        j = self.root(second)
        self._arr[i] = j

    def __str__(self):
        return str(self._arr)


def numislands(grid):
    # using unionfind, dfs, bfs
    quf = QUF() 


from pprint import pprint
def main():
    def togrid(inpstr):
        rows = [l for l in inpstr.splitlines() if l.strip()]
        grid = [list(row) for row in rows]
        return grid

    inpstr =  """
11110
11010
11000
00000
"""
    land = togrid(inpstr)
    pprint(land)

if __name__ == '__main__':
    main()
