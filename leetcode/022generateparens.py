
# Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

# For example, given n = 3, a solution set is:

# [
#   "((()))",
#   "(()())",
#   "(())()",
#   "()(())",
#   "()()()"
# ]

def generate_parens(n):
    if n == 0:
        return []
    else:
        return generate_helper('', n, 0)

def generate_helper(cur, numavailable, numopened):
    # close the rest..
    if numavailable==0:
        return [cur+ ")"*numopened]
    
    # if we don't have any open and still have pairs we didn't use
    if numopened == 0:
        return generate_helper(cur+"(", numavailable-1, numopened+1)
    
    # if we have opened we choose either to close it or to open a new one and decrement n
    return generate_helper(cur+"(", numavailable-1, numopened+1 ) + generate_helper(cur+")", numavailable, numopened-1)


def main():
    print(generate_parens(1))
    print(generate_parens(2))
    print(generate_parens(3))

if __name__ == '__main__':
    main()