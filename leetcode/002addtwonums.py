# You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

# You may assume the two numbers do not contain any leading zero, except the number 0 itself.

# Example:

# Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
# Output: 7 -> 0 -> 8
# Explanation: 342 + 465 = 807.

def addtwonums(lr1, lr2):
    # l1 = lr1[::-1]
    # l2 = lr2[::-1]
    res = []
    if len(lr1) > len(lr2):
        L1 = lr1
        L2 = lr2
    else:
        L1 = lr2 
        L2 = lr1

    carry = 0
    for i, n in enumerate(L1):
        n1 = n
        n2 = 0
        if len(L2) > i:
            n2 = L2[i]
        total = n1+n2 + carry
        carry = total // 10
        print("N1: {}, N2: {}, Carry: {}, Total: {}".format(n1, n2, carry, total))

        res.append(total%10)

    if carry > 0:
        res.append(carry)
    return res[::-1]

print(addtwonums([1,2,3], [4,5,6]))
print(addtwonums([3,4,2], [4,6]))
print(addtwonums([3,4,2], []))