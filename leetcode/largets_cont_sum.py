
def largest_cont_sum(arr):
    if not arr:
        return 0

    maxsum = cursum = arr[0]
    for num in arr[1:]:
        cursum = max(cursum+num, num)
        maxsum = max(maxsum, cursum)

    return maxsum



print(largest_cont_sum([1,2,-3,4,5,-2]))