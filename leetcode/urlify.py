# leetcode cracking the code..


def urlify(s):
    string = list(s)
    # count spaces
    spaces_count = string.count(' ') * 2
    string += [" "] * spaces_count

    reader = len(string) - spaces_count - 1
    writer = len(string) - 1

    while reader > 0 and writer > 0:
        cur = string[reader]
        if cur != ' ':
            string[writer] = cur
            writer -= 1
            reader -= 1
        else:
            string[writer] = '0'
            string[writer-1] = '2'
            string[writer-2] = '%'
            writer -= 3
            reader -= 1

    return "".join(string)


def main():
    print(urlify("hello world what's up?"))

if __name__ == '__main__':
    main()

