# Given a sorted integer array without duplicates, return the summary of its ranges.

# Example 1:

# Input:  [0,1,2,4,5,7]
# Output: ["0->2","4->5","7"]
# Explanation: 0,1,2 form a continuous range; 4,5 form a continuous range.
# Example 2:

# Input:  [0,2,3,4,6,8,9]
# Output: ["0","2->4","6","8->9"]
# Explanation: 2,3,4 form a continuous range; 8,9 form a continuous range.

def summary_ranges(nums):
    try:
        ranges_list = []
        i = 0
        while i < len(nums):
            j = i
            k = j + 1
            while nums[k]-nums[j] == 1 and j < len(nums):
                k += 1
                j += 1
            ranges_list.append("{} -> {}".format(nums[i], nums[j]))
            i = j + 1
    except Exception as e:
        print("ERR:" , e)
        import ipdb; ipdb.set_trace()
    return ranges_list

print(summary_ranges([0,1,2,4,5,7]))
print(summary_ranges([0,2,3,4,6,8,9]))


## RAISES..