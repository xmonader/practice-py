def reverse_int(n):
    stk = []

    # 482 % 10 => 2, 482//10 -> 48

    while n>10:
        stk.append(n%10)
        n //= 10
    stk.append(n)
    
    revnum = 0

    tenth = 1
    while stk:
        n = stk.pop()
        revnum += tenth*n
        tenth *= 10 
    return revnum


def main():
    n = 419
    print(reverse_int(n))

if __name__ == '__main__':
    main()