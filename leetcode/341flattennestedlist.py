# Given a nested list of integers, implement an iterator to flatten it.

# Each element is either an integer, or a list -- whose elements may also be integers or other lists.

# Example 1:

# Input: [[1,1],2,[1,1]]
# Output: [1,1,2,1,1]
# Explanation: By calling next repeatedly until hasNext returns false, 
#              the order of elements returned by next should be: [1,1,2,1,1].
# Example 2:

# Input: [1,[4,[6]]]
# Output: [1,4,6]
# Explanation: By calling next repeatedly until hasNext returns false, 
#              the order of elements returned by next should be: [1,4,6].


def flatten(inp):
    if inp == []:
        return []
    res = []
    for el in inp:
        if isinstance(el, list):
            res += flatten(el)
        else:
            res.append(el)
    return res

def main():
    print(flatten([[1,1],2,[1,1]]))
    print(flatten([1,[4,[6]]]))

if __name__ == '__main__':
    main()