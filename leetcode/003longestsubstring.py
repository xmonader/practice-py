# Given a string, find the length of the longest substring without repeating characters.

# Examples:

# Given "abcabcbb", the answer is "abc", which the length is 3.

# Given "bbbbb", the answer is "b", with the length of 1.

# Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, "pwke" is a subsequence and not a substring.


# abcabcbb

# >a:
#     b
#         c 
#             a< BREAK

# > b 
#     c
#         a
#             b< BREAK

# > c
#     a
#         b
#             c < BREAK
# > a
#     b 
#         c
#             b< BREAK 
    
def longestsubstringwithoutrepeatingcharacters(s):
    i = 0
    j = 0
    subs = []
    while i < len(s):
        sub = ""
        while j < len(s):
            if s[j] in sub:
                subs.append(sub)
                sub = ""
                break
            else:
                sub += s[j]
            j += 1
        i += 1
    return sorted(subs, key=len)[-1]

for t in ["abcabcbb", "hello", "pwwkew", "bbbbb"]:
    print(longestsubstringwithoutrepeatingcharacters(t))