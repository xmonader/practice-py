from collections import Counter

def isplaindrome_perm(s):
    c = Counter([x for x in s if x.isalnum()])
    countodds = 0
    if all(v%2 == 0 for v in c.values()):
        return False
    
    for v in c.values():
        if v%2 == 1:
            if countodds == 1:
                return False
            countodds += 1
    
    return True
    
def main():
    print(isplaindrome_perm("atcocta"))
    print(isplaindrome_perm("atcoctaa"))

if __name__ == '__main__':
    main()
    