# Given an array, rotate the array to the right by k steps, where k is non-negative.

# Example 1:

# Input: [1,2,3,4,5,6,7] and k = 3
# Output: [5,6,7,1,2,3,4]
# Explanation:
# rotate 1 steps to the right: [7,1,2,3,4,5,6]
# rotate 2 steps to the right: [6,7,1,2,3,4,5]
# rotate 3 steps to the right: [5,6,7,1,2,3,4]
# Example 2:

# Input: [-1,-100,3,99] and k = 2
# Output: [3,99,-1,-100]
# Explanation: 
# rotate 1 steps to the right: [99,-1,-100,3]
# rotate 2 steps to the right: [3,99,-1,-100]


from collections import deque

def rotatearray(arr, k=1):
    queue = deque()
    count = 0 
    while count < k and arr:
        queue.append( arr.pop())
        count += 1
    while queue:
        arr.insert(0, queue.popleft())

    return arr

arr = [1,2,3,4,5,6,7]
print(rotatearray(arr, 3))

# can use extra array with the correct indexing.. 

# public class Solution {
#     public void rotate(int[] nums, int k) {
#         int[] a = new int[nums.length];
#         for (int i = 0; i < nums.length; i++) {
#             a[(i + k) % nums.length] = nums[i];
#         }
#         for (int i = 0; i < nums.length; i++) {
#             nums[i] = a[i];
#         }
#     }
# }
